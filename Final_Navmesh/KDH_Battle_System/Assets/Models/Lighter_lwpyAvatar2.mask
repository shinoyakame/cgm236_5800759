%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Lighter_lwpyAvatar2
  m_Mask: 00000000000000000000000000000000000000000000000001000000000000000100000000000000000000000000000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/Root
    m_Weight: 1
  - m_Path: Armature/Root/FootIK.L
    m_Weight: 1
  - m_Path: Armature/Root/FootIK.L/KneeIK.L
    m_Weight: 1
  - m_Path: Armature/Root/FootIK.L/KneeIK.L/KneeIK.L_end
    m_Weight: 1
  - m_Path: Armature/Root/FootIK.R
    m_Weight: 1
  - m_Path: Armature/Root/FootIK.R/KneeIK.R
    m_Weight: 1
  - m_Path: Armature/Root/FootIK.R/KneeIK.R/KneeIK.R_end
    m_Weight: 1
  - m_Path: Armature/Root/HandIK.L
    m_Weight: 1
  - m_Path: Armature/Root/HandIK.L/ElbowIK.L
    m_Weight: 1
  - m_Path: Armature/Root/HandIK.L/ElbowIK.L/ElbowIK.L_end
    m_Weight: 1
  - m_Path: Armature/Root/HandIK.R
    m_Weight: 1
  - m_Path: Armature/Root/HandIK.R/ElbowIK.R
    m_Weight: 1
  - m_Path: Armature/Root/HandIK.R/ElbowIK.R/ElbowIK.R_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine/Chest
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine/Chest/Neck
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine/Chest/Neck/Head
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine/Chest/Neck/Head/Head_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine/Chest/Shoulder.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine/Chest/Shoulder.L/UpperArm.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine/Chest/Shoulder.L/UpperArm.L/LowerArm.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine/Chest/Shoulder.L/UpperArm.L/LowerArm.L/Hand.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine/Chest/Shoulder.L/UpperArm.L/LowerArm.L/Hand.L/Hand.L_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine/Chest/Shoulder.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine/Chest/Shoulder.R/UpperArm.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine/Chest/Shoulder.R/UpperArm.R/LowerArm.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine/Chest/Shoulder.R/UpperArm.R/LowerArm.R/Hand.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine/Chest/Shoulder.R/UpperArm.R/LowerArm.R/Hand.R/Hand.R_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Tail_0
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Tail_0/Tail_1
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Tail_0/Tail_1/Tail_2
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Tail_0/Tail_1/Tail_2/Tail_3
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Tail_0/Tail_1/Tail_2/Tail_3/Tail_3_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Thigh.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Thigh.L/Calf.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Thigh.L/Calf.L/Heel.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Thigh.L/Calf.L/Heel.L/Foot.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Thigh.L/Calf.L/Heel.L/Foot.L/Foot.L_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Thigh.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Thigh.R/Calf.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Thigh.R/Calf.R/Heel.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Thigh.R/Calf.R/Heel.R/Foot.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Thigh.R/Calf.R/Heel.R/Foot.R/Foot.R_end
    m_Weight: 1
  - m_Path: Gun
    m_Weight: 1
  - m_Path: Lighter_model
    m_Weight: 1
  - m_Path: Sweater
    m_Weight: 1
  - m_Path: Trousers
    m_Weight: 1
