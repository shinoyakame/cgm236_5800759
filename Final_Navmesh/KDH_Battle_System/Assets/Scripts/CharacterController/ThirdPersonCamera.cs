﻿//
// Unityちゃん用の三人称カメラ
// 
// 2013/06/07 N.Kobyasahi
//
using UnityEngine;
using System.Collections;


public class ThirdPersonCamera : MonoBehaviour
{
	public float smooth = 3f;	
	Transform standardPos;			// the usual position for the camera, specified by a transform in the game
	Transform frontPos;			// Front Camera locater
	Transform jumpPos;			// Jump Camera locater
	
	bool bQuickSwitch = false;	//Change Camera Position Quickly
    public static bool focused = false;
    public static Transform focusTransform;
    public Vector3 focusOffset;
	
	
	void Start()
	{
        focused = false;
        focusTransform = null;
        standardPos = GameObject.Find ("CamPos").transform;
		
		if(GameObject.Find ("FrontPos"))
			frontPos = GameObject.Find ("FrontPos").transform;

		if(GameObject.Find ("JumpPos"))
			jumpPos = GameObject.Find ("JumpPos").transform;

			transform.position = standardPos.position;	
			transform.forward = standardPos.forward;	
	}

	
	void Update ()
	{
        if (UIManager.endGame) return;
		if(Input.GetButtonDown("Fire2"))	// left Ctlr
		{
            if (!transform.parent.GetComponent<PlayerController>().isAiming)
            {
                // Change Front Camera
                if (transform.parent.GetComponent<PlayerController>().isGround)
                {
                    setCameraPositionFrontView();
                    transform.parent.GetComponent<IKHandling>().ikWeight = 1;
                    transform.parent.GetComponent<PlayerController>().isAiming = true;
                    focused = true;
                }
            }
            else
            {
                // return the camera to standard position and direction
                transform.parent.GetComponent<IKHandling>().ikWeight = 0;
                transform.parent.GetComponent<PlayerController>().isAiming = false;
                focused = false;
                focusTransform = null;
                setCameraPositionNormalView();
            }
        }

        if (Input.GetMouseButtonDown(0) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            CheckObjectClicked();
        }

        if (focused)
        {
            if (focusTransform == null)
            {
                if (UIManager.enemyList.Count <= 0)
                {
                    //gamewin
                    FindObjectOfType<UIManager>().EndGame();
                }
                else
                {
                    float nearestDist = float.PositiveInfinity;
                    Enemy nearestEnemy = UIManager.enemyList.First.Value;
                    foreach (Enemy en in UIManager.enemyList)
                    {
                        if (en == null)
                        {
                            //UIManager.enemyList.Remove(en);
                            continue;
                        }
                        float dist = Vector3.Distance(en.transform.position, transform.parent.position);
                        if (dist < nearestDist)
                        {
                            nearestDist = dist;
                            nearestEnemy = en;
                        }
                    }
                    //Debug.Log(nearestEnemy.transform);
                    if (nearestDist < 60)
                    {
                        focusTransform = nearestEnemy.transform;
                        focused = true;
                    }
                    else
                    {
                        focusTransform = null;
                        focused = false;
                        transform.parent.GetComponent<IKHandling>().ikWeight = 0;
                        transform.parent.GetComponent<PlayerController>().isAiming = false;
                        setCameraPositionNormalView();
                    }
                }
            }
            else
            {
                Vector3 lookPos = focusTransform.position - transform.parent.position;
                lookPos.y = 0;
                Quaternion rotation = Quaternion.LookRotation(lookPos);
                transform.parent.rotation = Quaternion.Slerp(transform.parent.rotation, rotation, 1f);
                if(Vector3.Distance(transform.parent.position, focusTransform.position) < 1f)
                {
                    focusTransform = null;
                    focused = false;
                    transform.parent.GetComponent<IKHandling>().ikWeight = 0;
                    transform.parent.GetComponent<PlayerController>().isAiming = false;
                    setCameraPositionNormalView();
                }
            }
        }

        
	}

    void CheckObjectClicked()
    {
        Ray interactionRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit interactionInfo;
        if (Physics.Raycast(interactionRay, out interactionInfo, Mathf.Infinity))
        {
            GameObject interactedObject = interactionInfo.collider.gameObject;
            if (interactedObject.tag == "Enemy")
            {
                //focus on enemy
                focusTransform = interactedObject.transform;
                focused = true;
            }
            else
            {
                //unfocus
                focusTransform = null;
                focused = false;
                transform.parent.GetComponent<IKHandling>().ikWeight = 0;
                transform.parent.GetComponent<PlayerController>().isAiming = false;
                setCameraPositionNormalView();
            }
        }
    }

    void setCameraPositionNormalView()
	{
		if(bQuickSwitch == false){
		// the camera to standard position and direction
						transform.position = Vector3.Lerp(transform.position, standardPos.position, Time.fixedDeltaTime * smooth);	
						transform.forward = Vector3.Lerp(transform.forward, standardPos.forward, Time.fixedDeltaTime * smooth);
		}
		else{
			// the camera to standard position and direction / Quick Change
			transform.position = standardPos.position;	
			transform.forward = standardPos.forward;
			bQuickSwitch = false;
		}
	}

	
	void setCameraPositionFrontView()
	{
		// Change Front Camera
		bQuickSwitch = true;
		transform.position = frontPos.position;	
		transform.forward = frontPos.forward;
	}

	void setCameraPositionJumpView()
	{
		// Change Jump Camera
		bQuickSwitch = false;
				transform.position = Vector3.Lerp(transform.position, jumpPos.position, Time.fixedDeltaTime * smooth);	
				transform.forward = Vector3.Lerp(transform.forward, jumpPos.forward, Time.fixedDeltaTime * smooth);		
	}
}
