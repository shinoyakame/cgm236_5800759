﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Animator))]
[RequireComponent(typeof (CapsuleCollider))]
[RequireComponent(typeof (Rigidbody))]

public class PlayerController : Character
{
    public ThirdPersonCamera thirdPersonCamera;
    public bool isGround;
    Vector3 savedPosition;

    //Bullet Shoot
    public float bulletSpeed;
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public Transform upperArm;
    

    public override void Start ()
	{
        base.Start();
        isAiming = false;
        isGround = true;
        savedPosition = transform.position;
    }
	
    void OnCollisionEnter(Collision other)
    {
        isGround = true;
        savedPosition = other.collider.transform.position;
    }

    void SpawnBullet()
    {
        GameObject bullet = Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity) as GameObject;
        Vector3 temp = Vector3.Normalize(bulletSpawn.position - upperArm.position);
        temp.y = 0;
        bullet.GetComponent<Bullet>().direction = Vector3.Normalize(temp);
        bullet.GetComponent<Bullet>().speed = bulletSpeed;
        bullet.GetComponent<Bullet>().player = transform;
    }

	public override void FixedUpdate ()
	{
        if (UIManager.endGame) return;
        if (transform.position.y < savedPosition.y - 30)
        {
            transform.position = savedPosition + Vector3.up * 20;
            return;
        }

        h = Input.GetAxis("Horizontal") * (isAiming? 0.8f : 1f);
        v = Input.GetAxis("Vertical") * (isAiming ? 0.8f : 1f);
        rotateSpeed = 2.0f * (isAiming ? 8f : 1f);

        base.FixedUpdate();

        transform.localPosition += velocity * Time.fixedDeltaTime;

        transform.Rotate(0, h * rotateSpeed, 0);
    }

    public override void CalculateVelocity(float h, float v)
    {
        if (ThirdPersonCamera.focused)
        {
            velocity = new Vector3(h, 0, v);
            velocity = transform.TransformDirection(velocity);
            velocity *= forwardSpeed;
        }
        else
        {
            velocity = new Vector3(0, 0, v);
            velocity = transform.TransformDirection(velocity);
            if (v > 0.1)
            {
                velocity *= forwardSpeed;
            }
            else if (v < -0.1)
            {
                velocity *= backwardSpeed;
            }
        }
    }

    public override void StateLoco()
    {
        base.StateLoco();
        if (Input.GetButtonDown("Jump"))
        {
            if (isGround)
            {
                if (isAiming)
                {
                    SpawnBullet();
                }
                else
                {
                        rb.AddForce(Vector3.up * jumpPower, ForceMode.VelocityChange);
                        anim.SetBool("Jump", true);
                        isGround = false;
                }
            }
        }
    }

    public override void StateIdle()
    {
        base.StateIdle();
        if (Input.GetButtonDown("Jump") && isGround)
        {
            if (isAiming)
            {
                SpawnBullet();
            }
            else
            {
                    rb.AddForce(Vector3.up * jumpPower, ForceMode.VelocityChange);
                    anim.SetBool("Jump", true);
                    anim.Play("Jump");
                    isGround = false;
            }
        }
    }
}
