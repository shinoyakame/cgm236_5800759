﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Rigidbody))]

public class Character : MonoBehaviour {

    public float animSpeed = 1.5f;
    public bool useCurves = true;
    public float useCurvesHeight = 0.5f;

    public float forwardSpeed = 20.0f;
    public float backwardSpeed = 20.0f;
    public float rotateSpeed = 2.0f;
    public float jumpPower = 3.0f;
    public float maxhp;
    public float hp;
    public int index;

    public bool isAiming;

    internal CapsuleCollider col;
    internal Rigidbody rb;
    internal Vector3 velocity;
    internal float orgColHight;
    internal Vector3 orgVectColCenter;

    internal Animator anim;
    internal AnimatorStateInfo currentBaseState;

    internal int idleState = Animator.StringToHash("Base Layer.Idle");
    internal int locoState = Animator.StringToHash("Base Layer.Locomotion");
    internal int walkBackState = Animator.StringToHash("Base Layer.WalkBack");
    internal int jumpState = Animator.StringToHash("Base Layer.Jump");
    internal int restState = Animator.StringToHash("Base Layer.Rest");

    internal float h, v;

    public virtual void Start()
    {
        anim = GetComponent<Animator>();
        col = GetComponent<CapsuleCollider>();
        rb = GetComponent<Rigidbody>();
        orgColHight = col.height;
        orgVectColCenter = col.center;
        hp = maxhp;
    }

    public virtual void FixedUpdate()
    {
        if (UIManager.endGame) return;
        rb.useGravity = true;
        AnimatorUpdate(h, v);

        CalculateVelocity(h, v);

        CheckAnimationState();
    }

    public virtual void AnimatorUpdate(float h, float v)
    {
        anim.SetFloat("Speed", (Mathf.Abs(h)*0.6f+v));
        anim.SetFloat("Direction", h);
        anim.speed = animSpeed;
        currentBaseState = anim.GetCurrentAnimatorStateInfo(0);
    }

    public virtual void CalculateVelocity(float h, float v)
    {
        
    }

    public void CheckAnimationState()
    {
        if (currentBaseState.fullPathHash == locoState)
        {
            StateLoco();
        }
        else if (currentBaseState.fullPathHash == jumpState)
        {
            StateJump();
        }
        else if (currentBaseState.fullPathHash == idleState)
        {
            StateIdle();
        }
        else if (currentBaseState.fullPathHash == restState)
        {
            StateRest();
        }
        else if (currentBaseState.fullPathHash == walkBackState)
        {
            StateLoco();
        }
    }

    public virtual void StateLoco()
    {
        if (useCurves)
        {
            resetCollider();
        }
    }

    public virtual void StateJump()
    {
        //if (!anim.IsInTransition(0))
        //{
            if (useCurves)
            {
                float jumpHeight = anim.GetFloat("JumpHeight");
                float gravityControl = anim.GetFloat("GravityControl");
                if (gravityControl > 0)
                    rb.useGravity = false;

                Ray ray = new Ray(transform.position + Vector3.up, -Vector3.up);
                RaycastHit hitInfo = new RaycastHit();
                if (Physics.Raycast(ray, out hitInfo))
                {
                    if (hitInfo.distance > useCurvesHeight)
                    {
                        col.height = orgColHight - jumpHeight;
                        float adjCenterY = orgVectColCenter.y + jumpHeight;
                        col.center = new Vector3(0, adjCenterY, 0);
                    }
                    else
                    {
                        resetCollider();
                    }
                }
            }
            anim.SetBool("Jump", false);

        //}
    }

    public virtual void StateIdle()
    {
        if (useCurves)
        {
            resetCollider();
        }
    }

    public virtual void StateRest()
    {
        if (!anim.IsInTransition(0))
        {
            anim.SetBool("Rest", false);
        }
    }

    public void resetCollider()
    {
        col.height = orgColHight;
        col.center = orgVectColCenter;
    }
}
