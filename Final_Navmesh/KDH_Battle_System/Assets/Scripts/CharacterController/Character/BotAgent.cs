﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(NavMeshAgent))]

public class BotAgent : Character {

    public Transform target;
    public NavMeshAgent navMeshAgent;
    public float distance;

    //Bullet Shoot
    public float bulletSpeed;
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public Transform upperArm;

    public enum Behaviour
    {
        Attack,
        Follow
    }
    public Behaviour behaviour = new Behaviour();

    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    public override void Start()
    {
        base.Start();
        navMeshAgent.stoppingDistance = distance;
    }

    public override void FixedUpdate()
    {
        if (target != null)
        {
            navMeshAgent.destination = target.position;
        }
        else
        {
            navMeshAgent.path.ClearCorners();
            target = FindObjectOfType<PlayerController>().transform;
        }
        h = navMeshAgent.velocity.x;
        v = navMeshAgent.velocity.z;
        base.FixedUpdate();
    }

    public override void AnimatorUpdate(float h, float v)
    {
        base.AnimatorUpdate(h, v);
        anim.SetBool("Jump", navMeshAgent.isOnOffMeshLink);
    }

    public override void CalculateVelocity(float h, float v)
    {
        velocity = new Vector3(0, 0, v);
        velocity = transform.TransformDirection(velocity);

        if (v > 0.1)
        {
            velocity *= forwardSpeed;
        }
        else if (v < -0.1)
        {
            velocity *= backwardSpeed;
        }
    }
    

    internal void SpawnBullet()
    {
        GameObject bullet = Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity) as GameObject;
        Vector3 temp = Vector3.Normalize(bulletSpawn.position - upperArm.position);
        temp.y = 0;
        bullet.GetComponent<Bullet>().direction = Vector3.Normalize(temp);
        bullet.GetComponent<Bullet>().speed = bulletSpeed;
    }

}
