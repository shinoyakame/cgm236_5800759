﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : BotAgent {

    public override void Start()
    {
        base.Start();
        behaviour = Behaviour.Follow;
        FindPlayer();
        StartCoroutine(Attack());
        navMeshAgent.stoppingDistance = 0;
    }

    internal IEnumerator Attack()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            if (behaviour == Behaviour.Attack)
            {
                ChooseTarget();
                WanderAI();
                //Shoot
                for (int i = 0; i < 3; i++)
                {
                    SpawnBullet();
                    yield return new WaitForSeconds(0.1f);
                }
            }
            else if(behaviour == Behaviour.Follow)
            {
                if (CalculateNearestPlayer() != null)
                {
                    //Debug.Log("ok");
                    if (Vector3.Distance(CalculateNearestPlayer().transform.position, transform.position) <= 50)
                    {
                        behaviour = Behaviour.Attack;
                    }
                }
            }
        }
    }

    void WanderAI()
    {
        Vector3 randDirection = Random.insideUnitSphere * 20; //wander radius = 10
        randDirection += transform.position;
        NavMeshHit navHit;
        NavMesh.SamplePosition(randDirection, out navHit, 20, -1);
        Vector3 newPos = navHit.position;
        navMeshAgent.SetDestination(newPos);

    }

    void FindPlayer()
    {
        PlayerController[] temp = GameObject.FindObjectsOfType<PlayerController>();
        for (int i = 0; i < temp.Length; i++)
        {
            if (temp[i].enabled)
            {
                target = temp[i].transform;
                break;
            }
        }
    }

    void ChooseTarget()
    {
        if (CalculateNearestPlayer() != null) {
            target = CalculateNearestPlayer().transform;
        }
        else
        {
            //behaviour = Behaviour.Follow;
            target = FindObjectOfType<PlayerController>().transform;
        }
    }

    internal Character CalculateNearestPlayer()
    {
        float nearestDist = float.PositiveInfinity;
        Character[] enelist = new Character[3];
        enelist[0] = FindObjectOfType<PlayerController>();
        enelist[1] = FindObjectOfType<BotGelix>();
        enelist[2] = FindObjectOfType<BotShadowman>();
        Character nearestene;
        nearestene = enelist[0];
        for(int i=0; i<enelist.Length; i++)
        {
            //if (ene.CompareTag("Enemy"))
            //{
            //Debug.Log(enelist[i].name);
            //continue;
            //}
            if (enelist[i] == null)
            {
                continue;
            }
            float dist = Vector3.Distance(enelist[i].transform.position, transform.position);
            if (dist < nearestDist)
            {
                nearestDist = dist;
                nearestene = enelist[i];
            }
            
        }
        if (nearestDist > 60)
        {
            //Debug.Log(nearestene.name);
            return null;
        }
        else
        {
            return nearestene;
        }
    }

    void OnTriggerStay(Collider collider)
    {
        if (collider.CompareTag("Player"))
        {
            float damage = 400;
            if (collider.GetComponent<PlayerController>() != null) //Lighter
            {
                PlayerController player = collider.transform.GetComponent<PlayerController>();
                player.hp -= Time.deltaTime * damage;
                FindObjectOfType<UIManager>().UpdateHeroHP(player.index);
            }
            else if (collider.GetComponent<BotGelix>() != null) //Gelix
            {
                BotGelix player = collider.transform.GetComponent<BotGelix>();
                player.hp -= Time.deltaTime * damage;
                FindObjectOfType<UIManager>().UpdateHeroHP(player.index);
            }else if (collider.GetComponent<BotShadowman>() != null) //Shadowman
            {
                BotShadowman player = collider.transform.GetComponent<BotShadowman>();
                player.hp -= Time.deltaTime * damage;
                FindObjectOfType<UIManager>().UpdateHeroHP(player.index);
            }else
            {
                PlayerController player = collider.transform.parent.GetComponent<PlayerController>();
                player.hp -= Time.deltaTime * damage;
                FindObjectOfType<UIManager>().UpdateHeroHP(player.index);
            }
        }
    }

}
