﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BotTeamMember : BotAgent {

    internal float distanceFromPlayer;
    internal Transform nearestCharacter;
    internal Transform player;

    public override void Start()
    {
        base.Start();
        player = FindObjectOfType<PlayerController>().transform;
        behaviour = Behaviour.Follow;
        FindPlayer();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        distanceFromPlayer = Vector3.Distance(transform.position, player.position);
        CalculateNearestCharacter();
    }

    void FindPlayer()
    {
        PlayerController[] temp = GameObject.FindObjectsOfType<PlayerController>();
        for (int i=0; i<temp.Length; i++)
        {
            if (temp[i].enabled)
            {
                target = temp[i].transform;
                break;
            }
        }
    }

    internal void CalculateNearestCharacter()
    {
        float nearestDist = float.PositiveInfinity;
        Character[] chalist = FindObjectsOfType<Character>();
        foreach (Character cha in chalist)
        {
            float dist = Vector3.Distance(cha.transform.position, transform.position);
            if (dist < nearestDist)
            {
                nearestDist = dist;
                nearestCharacter = cha.transform;
            }
        }
    }

    public override void CalculateVelocity(float h, float v)
    {
        if (behaviour == Behaviour.Attack)
        {
            isAiming = true;
            GetComponent<IKHandling>().ikWeight = 1;
        }
        else if (behaviour == Behaviour.Follow)
        {
            isAiming = false;
            GetComponent<IKHandling>().ikWeight = 0;
        }
    }

    internal Enemy CalculateNearestEnemy()
    {

        float nearestDist = float.PositiveInfinity;
        Enemy[] enelist = FindObjectsOfType<Enemy>();
        Enemy nearestene;
        if (enelist.Length > 0)
        {
            nearestene = enelist[0];
            foreach (Enemy ene in enelist)
            {
                float dist = Vector3.Distance(ene.transform.position, transform.position);
                if (dist < nearestDist)
                {
                    nearestDist = dist;
                    nearestene = ene;
                }
            }
            if (nearestDist > 30)
            {
                return null;
            }
            else
            {
                return nearestene;
            }
        }
        else
        {
            return null;
        }
    }



    internal void ChooseTarget()
    {
        if (behaviour == Behaviour.Follow)
        {
            target = player;
            navMeshAgent.stoppingDistance = 5;
        }
        else if (behaviour == Behaviour.Attack)
        {
            if (CalculateNearestEnemy()==null)
            {
                behaviour = Behaviour.Follow;
            }
            else
            {
                target = CalculateNearestEnemy().transform;
            }
            transform.TransformDirection(target.position);
            navMeshAgent.stoppingDistance = 2;
        }
    }

    internal IEnumerator Attack()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            if (behaviour == Behaviour.Attack)
            {
                ChooseTarget();
                //Shoot
                for (int i = 0; i < 3; i++)
                {
                    SpawnBullet();
                    yield return new WaitForSeconds(0.1f);
                }
            }
        }
    }

}
