﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotShadowman : BotTeamMember {

    float FuzzyLogic()
    {
        float aggressive = 0;
        //ลุยแหลก
        //distance from player > 15 :: Follow 0.4
        //distance from player < 15 :: Attack 1.0
        float a = distanceFromPlayer > 15 ? 0.4f : 1.0f;
        //nearest character = player :: Follow 0.3
        //nearest character = enemy :: Attack 0.8
        float b = nearestCharacter.CompareTag("Player")? 0.3f:0.8f;
        aggressive = a + b / 2;
        if (CalculateNearestEnemy() == null)
        {
            aggressive = 0;
        }
        return aggressive;
    }

    public override void Start()
    {
        base.Start();
        StartCoroutine(Decision());
        StartCoroutine(Attack());
    }

    IEnumerator Decision()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(0.5f,1.5f));
            if (FuzzyLogic() > 0.5f) //Aggressive
            {
                behaviour = Behaviour.Attack;
            }
            else
            {
                behaviour = Behaviour.Follow;
            }
        }
    }

}
