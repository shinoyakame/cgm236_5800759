﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotGelix : BotTeamMember {

	float FuzzyLogic()
    {
        float aggressive = 0;
        //ห่วงผู้เล่น
        //distance from player > 15 :: Follow 0.1
        //distance from player < 15 :: Attack 0.6
        float a = distanceFromPlayer > 15 ? 0.1f : 0.6f;
        //nearest character = player :: Follow 0.4
        //nearest character = enemy :: Attack 0.7
        float b = nearestCharacter.CompareTag("Player") ? 0.4f : 0.7f;
        aggressive = a + b / 2;
        if (CalculateNearestEnemy() == null)
        {
            aggressive = 0;
        }
        return aggressive;
    }

    public override void Start()
    {
        base.Start();
        StartCoroutine(Decision());
        StartCoroutine(Attack());
    }

    IEnumerator Decision()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));
            if (FuzzyLogic() > 0.5f) //Aggressive
            {
                behaviour = Behaviour.Attack;
            }
            else
            {
                behaviour = Behaviour.Follow;
            }
        }
    }

}
