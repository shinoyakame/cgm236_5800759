﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

    public bool x, y, z;
    public float speed;

    void Update()
    {
        transform.Rotate(new Vector3(x ? speed : 0, y ? speed : 0, z ? speed : 0)*Time.deltaTime);
    }

}
