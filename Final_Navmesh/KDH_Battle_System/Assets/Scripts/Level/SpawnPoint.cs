﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {

    bool spawned;
    public Transform[] spawnpoint;

    void Start()
    {
        for (int i = 0; i < spawnpoint.Length; i++)
        {
            spawnpoint[i].GetChild(0).GetComponent<Enemy>().navMeshAgent.isStopped = true;
        }
    }

	void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Player") && !spawned)
        {
            for(int i=0; i<spawnpoint.Length; i++)
            {
                spawnpoint[i].GetChild(0).GetComponent<Enemy>().navMeshAgent.isStopped = false;
            }
            spawned = true;
        }
    }

}
