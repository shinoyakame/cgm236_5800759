﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    public RectTransform lighterHp, gelixHp, shadowmanHp, enemyHp;
    public Character lighterCC, gelixCC, shadowmanCC, enemyCC;
    public GameObject enemyHpBar;
    public static LinkedList<Enemy> enemyList;
    public float lighterHpw, gelixHpw, shadowmanHpw, enemyHpw;
    public GameObject endPanel;
    public static bool endGame;

    void Start()
    {
        endGame = false;
        enemyHpBar.SetActive(false);
        GetHPBarWidth();
        UpdateEnemyList();
    }

    void GetHPBarWidth()
    {
        lighterHpw = lighterHp.sizeDelta.x;
        gelixHpw = gelixHp.sizeDelta.x;
        shadowmanHpw = shadowmanHp.sizeDelta.x;
        enemyHpw = enemyHp.sizeDelta.x;
    }

    void UpdateEnemyList()
    {
        Enemy[] temp = FindObjectsOfType<Enemy>();
        enemyList = new LinkedList<Enemy>();
        enemyList.Clear();
        for (int i=0; i<temp.Length; i++)
        {
            temp[i].index = i;
            enemyList.AddLast(temp[i]);
        }
    }

    public void UpdateHeroHP(int index)
    {
        switch (index)
        {
            case 0:
                lighterHp.sizeDelta = new Vector2(lighterCC.hp / lighterCC.maxhp * lighterHpw, lighterHp.sizeDelta.y);
                if (lighterHp.sizeDelta.x <= 0)
                {
                    EndGame();
                }
                break;
            case 1:
                if (gelixCC == null) break;
                gelixHp.sizeDelta = new Vector2(gelixCC.hp / gelixCC.maxhp * gelixHpw, gelixHp.sizeDelta.y);
                if (gelixHp.sizeDelta.x <= 0)
                {
                    Destroy(gelixCC.gameObject);
                }
                break;
            case 2:
                if (shadowmanCC == null) break;
                shadowmanHp.sizeDelta = new Vector2(shadowmanCC.hp / shadowmanCC.maxhp * shadowmanHpw, shadowmanHp.sizeDelta.y);
                if (shadowmanHp.sizeDelta.x <= 0)
                {
                    Destroy(shadowmanCC.gameObject);
                }
                break;
            default:
                break;
        }
    }

    public void UpdateEnemyHP(int index)
    {
        if (enemyList.Count-1 < index) return;
        enemyCC = EnemyLinkedListIterator(index);
        StartCoroutine(UpdateEnemyHp());
    }

    Enemy EnemyLinkedListIterator(int index)
    {
        LinkedListNode<Enemy> output = enemyList.First;
        for(int i=0; i<index; i++)
        {
            output = output.Next;
        }
        return output.Value;
    }

    IEnumerator UpdateEnemyHp()
    {
        enemyHpBar.SetActive(true);
        enemyHp.sizeDelta = new Vector2(enemyCC.hp / enemyCC.maxhp * enemyHpw, enemyHp.sizeDelta.y);
        yield return new WaitForSeconds(5f);
        enemyHpBar.SetActive(false);
    }

    public void EndGame()
    {
        endGame = true;
        endPanel.SetActive(true);
        Invoke("Restart", 3.25f);
    }

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
