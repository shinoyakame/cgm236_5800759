﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public bool player;
    public float damage = 10;
    public float speed;
    public Vector3 direction;

    void Start()
    {
        if (ThirdPersonCamera.focusTransform != null)
        {
            Vector3 temp = Vector3.Normalize(ThirdPersonCamera.focusTransform.position - transform.position);
            temp.y = 0;
            direction = transform.TransformDirection(Vector3.Normalize(temp));
        }
        Destroy(gameObject, 10f);
    }

    void FixedUpdate()
    {
        Ray ray = new Ray(transform.position, direction);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit, speed*Time.fixedDeltaTime))
        {
            //hit enemy
            //Debug.Log("Hit");
            if (hit.collider.CompareTag("Enemy") && player)
            {
                Enemy enemy;
                if(hit.collider.GetComponent<Enemy>()!= null)
                {
                    enemy = hit.collider.GetComponent<Enemy>();
                }
                else
                {
                    enemy = hit.collider.transform.parent.GetComponent<Enemy>();
                }
                enemy.hp -= damage;
                if (enemy.hp <= 0)
                {
                    int index = enemy.index;
                    UIManager.enemyList.Remove(enemy);
                    foreach (Enemy en in UIManager.enemyList)
                    {
                        if (en.index > index)
                        {
                            en.index--;
                        }
                    }
                    Destroy(enemy.gameObject);
                }
                FindObjectOfType<UIManager>().UpdateEnemyHP(enemy.index);
            }
            else if (hit.collider.CompareTag("Player") && !player)
            {
                Character victim;
                if (hit.collider.GetComponent<Character>() != null)
                {
                    victim = hit.collider.GetComponent<Character>();
                }
                else
                {
                    victim = hit.collider.transform.parent.GetComponent<Character>();
                }
                victim.hp -= damage;
                if (victim.hp <= 0)
                {
                    //player death
                    if (victim.index == 0)
                    {
                        FindObjectOfType<UIManager>().EndGame();
                    }
                    else
                    {
                        Destroy(victim.gameObject);
                    }
                }
                FindObjectOfType<UIManager>().UpdateHeroHP(victim.index);

            }
            //destroybullet
            Destroy(gameObject);
        }
        transform.Translate(direction * speed * Time.fixedDeltaTime);
    }

    /*void OnTriggerEnter(Collider hit)
    {
        if (hit.CompareTag("Enemy") && player)
        {
            Enemy enemy;
            if (hit.GetComponent<Enemy>() != null)
            {
                enemy = hit.GetComponent<Enemy>();
            }
            else
            {
                enemy = hit.transform.parent.GetComponent<Enemy>();
            }
            enemy.hp -= damage;
            if (enemy.hp <= 0)
            {
                int index = enemy.index;
                UIManager.enemyList.Remove(enemy);
                foreach (Enemy en in UIManager.enemyList)
                {
                    if (en.index > index)
                    {
                        en.index--;
                    }
                }
                Destroy(enemy.gameObject);
            }
            FindObjectOfType<UIManager>().UpdateEnemyHP(enemy.index);
        }
        else if (hit.CompareTag("Player") && !player)
        {
            Character victim;
            Debug.Log("yee");
            if (hit.GetComponent<Character>() != null)
            {
                victim = hit.GetComponent<Character>();
            }
            else
            {
                victim = hit.transform.parent.GetComponent<Character>();
            }
            victim.hp -= damage;
            if (victim.hp <= 0)
            {
                //player death
                if (victim.index == 0)
                {
                    FindObjectOfType<UIManager>().EndGame();
                }
                else
                {
                    Destroy(victim.gameObject);
                }
            }
            FindObjectOfType<UIManager>().UpdateHeroHP(victim.index);

        }
        //destroybullet
        Destroy(gameObject);
    }*/

}
