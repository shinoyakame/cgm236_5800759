﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week02_CustomBST
{
    class Program
    {
        static void Main(string[] args)
        {
            BinarySearchTree nums = new BinarySearchTree();
            nums.Insert(23);
            nums.Insert(45);
            nums.Insert(16);
            nums.Insert(37);
            nums.Insert(3);
            nums.Insert(99);
            nums.Insert(22);

            //Console.WriteLine("BST : InOrder");
            //nums.InOrder(nums.root);
            //Console.WriteLine();

            //Console.WriteLine("BST : PreOrder");
            //nums.PreOrder(nums.root);
            //Console.WriteLine();

            //Console.WriteLine("BST : PostOrder");
            //nums.PostOrder(nums.root);
            //Console.WriteLine();

            Console.Write("MIN IS : ");
            Console.WriteLine(nums.FindMin());
            Console.WriteLine();

            Console.Write("MAX IS : ");
            Console.WriteLine(nums.FindMax());
            Console.WriteLine();

            Console.Read();

        }
    }
}
