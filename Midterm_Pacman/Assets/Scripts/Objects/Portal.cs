﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {

    public Transform portal;
    public Vector3 spawnOffset;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Player"))
        {
            collider.transform.position = portal.position + spawnOffset;
        }
        if (collider.CompareTag("Enemy"))
        {
            collider.transform.position = portal.position + spawnOffset;
        }
    }

}
