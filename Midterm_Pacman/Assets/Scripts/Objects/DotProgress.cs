﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DotProgress : MonoBehaviour {

    public static DotProgress dontDestroy;

	void Awake()
    {
        if (dontDestroy == null)
        {
            dontDestroy = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (dontDestroy != this)
        {
            Destroy(gameObject);
        }
    }

}
