﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour {

    public Node[] nextNode = new Node[4];
    public bool isChecked;
    public Node parent;

}
