﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character {

    public float scatterTime;
    public float chaseTime;
    public Transform player;
    public enum Ghost
    {
        Blinky,
        Clyde,
        Inky,
        Pinky
    }
    public Ghost ghost;
    public Transform startNode;
    public Vector3 target;
    public bool active, started;
    public enum State
    {
        StandBy,
        Scattered,
        Chased,
        Retreat,
        ToStart
    }
    public State state;
    private State currentState;
    bool speedUp;
    public Stack<int> path;
    public EnemyNev enemyNev;

    private Direction oppDir;

    public Direction OppDir
    {
        get{
            return oppDir;
        }
        set{
            switch (value)
            {
                case Direction.Left:
                    oppDir = Direction.Right;
                    return;
                case Direction.Right:
                    oppDir = Direction.Left;
                    return;
                case Direction.Up:
                    oppDir = Direction.Down;
                    return;
                case Direction.Down:
                    oppDir = Direction.Up;
                    return;
                default:
                    return;
            }
        }
    }

    public EnemySound enemySound;

    public override void Start()
    {
        base.Start();
        path = new Stack<int>();
    }
    public override void Update()
    {
        base.Update();
        if (!GameManager.play)
        {
            return;
        }
        if (GameManager.death)
        {
            enemySound.StopAll();
        }
        if (GameProgress.score >= 1300) { 
            speedUp = true;
        }
        ToggleState();
    }

    IEnumerator ToggleChased()
    {
        yield return new WaitForSeconds(scatterTime);
        state = State.Chased;
    }

    IEnumerator ToggleScattered()
    {
        yield return new WaitForSeconds(chaseTime);
        state = State.Scattered;
    }

    void ToggleState()
    {
        if (currentState != state)
        {
            enemySound.StopAll();
            currentState = state;
            if (state == State.Scattered)
            {
                StartCoroutine("ToggleChased");
            }
            else if (state == State.Chased)
            {
                StartCoroutine("ToggleScattered");
            }
            else
            {
                StopCoroutine("ToggleChased");
                StopCoroutine("ToggleScattered");
            }
        }
        switch (state)
        {
            case State.StandBy:
                break;
            case State.ToStart:
                speed = 1.2f;
                enemySound.Play(3);
                base.Move();
                break;
            case State.Scattered:
                speed = 0.9f;
                base.Move();
                if(!speedUp) enemySound.Play(0);
                else enemySound.Play(1);
                break;
            case State.Chased:
                speed = 0.9f;
                base.Move();
                if (!speedUp) enemySound.Play(0);
                else enemySound.Play(1);
                break;
            case State.Retreat:
                speed = 0.4f;
                base.Move();
                enemySound.Play(2);
                break;
        }
    }

    public IEnumerator Eaten()
    {
        animator.SetTrigger("death");
        state = State.ToStart;
        GameManager.play = false;
        enemySound.Play(4);
        player.gameObject.SetActive(false);
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        yield return new WaitForSeconds(1f);
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        player.gameObject.SetActive(true);
        enemySound.Stop(4);
        enemySound.level = 3;
        GameManager.play = true;
    }

    public override void Move()
    {
        if (GameManager.death) return;
        Vector3 forward = Vector3.zero;
        switch (moveDir)
        {
            case Direction.Left:

                forward = Vector3.left;
                isHorizontalLen = true;
                nextNode = lastNode.nextNode[0];
                if (node != null)
                {
                    if (node.nextNode[0] == null)
                    {
                        Scattered();
                    }
                }
                break;
            case Direction.Right:


                forward = Vector3.right;
                isHorizontalLen = true;
                nextNode = lastNode.nextNode[1];
                if (node != null)
                {
                    if (node.nextNode[1] == null)
                    {
                        Scattered();
                    }
                }
                break;
            case Direction.Down:
                forward = Vector3.down;
                isHorizontalLen = false;
                nextNode = lastNode.nextNode[3];
                if (node != null)
                {
                    if (node.nextNode[3] == null)
                    {
                        Scattered();
                    }
                }
                break;
            case Direction.Up:
                forward = Vector3.up;
                isHorizontalLen = false;
                nextNode = lastNode.nextNode[2];
                if (node != null)
                {
                    if (node.nextNode[2] == null)
                    {
                        Scattered();
                    }
                }
                break;
            default:
                break;
        }
        transform.Translate(forward * speed * Time.deltaTime);
    }


    public override void OnTriggerEnter2D(Collider2D collider)
    {
        base.OnTriggerEnter2D(collider);
        if (collider.CompareTag("Node"))
        {
            if (state == State.ToStart) ToStart();
            if (state == State.Scattered) Scattered();
            if (state == State.Chased) Chased();
            if (state == State.Retreat) Retreat();
        }
    }

    public override void OnTriggerStay2D(Collider2D collider)
    {
        base.OnTriggerStay2D(collider);
        if (collider.transform == startNode && state == State.ToStart)
        {
            transform.position = collider.transform.position;
            animator.SetTrigger("birth");
            node = collider.GetComponent<Node>();
            keyDir = Direction.Left;
            state = State.Scattered;
        }
    }

    public override void OnTriggerExit2D(Collider2D collider)
    {
        base.OnTriggerExit2D(collider);
    }

    void Scattered()
    {
        OppDir = keyDir;
        bool[] canMove = new bool[4];
        for (int i = 0; i < canMove.Length; i++)
        {
            //left right up down
            canMove[i] = (node.nextNode[i] != null);
        }
        do
        {
            keyDir = (Direction)Random.Range(0, 4); //0 left, 1 right, 2 up, 3 down
        } while (!canMove[(int)keyDir] || OppDir == keyDir);
    }

    void Chased()
    {
        while (path.Count > 0)
        {
            path.Pop();
        }
        int[] directionData;
        switch (ghost)
        {
            case Ghost.Blinky:
                if (node == null) return;
                directionData = enemyNev.FindPath(node, player.GetComponent<PlayerManager>().lastNode);
                for (int i = directionData.Length - 1; i > -1; i--)
                {
                    path.Push(directionData[i]);
                }
                if (path.Count > 0)
                {
                    keyDir = (Direction)path.Pop();
                }
                break;
            case Ghost.Pinky:
                if (node == null) return;
                Node targetNode = player.GetComponent<PlayerManager>().lastNode;
                if (player.GetComponent<PlayerManager>().nextNode != null)
                {
                    targetNode = player.GetComponent<PlayerManager>().nextNode;
                }
                directionData = enemyNev.FindPath(node, targetNode);
                for (int i = directionData.Length - 1; i > -1; i--)
                {
                    path.Push(directionData[i]);
                }
                if (path.Count > 0)
                {
                    keyDir = (Direction)path.Pop();
                }
                break;
            case Ghost.Clyde:
                if (node == null) return;
                directionData = enemyNev.FindPath(node, player.GetComponent<PlayerManager>().lastNode);
                for (int i = directionData.Length - 1; i > -1; i--)
                {
                    path.Push(directionData[i]);
                }
                if (path.Count > 1)
                {
                    keyDir = (Direction)path.Pop();
                }
                else
                {
                    state = State.Scattered;
                }
                break;
            case Ghost.Inky:
                if (node == null) return;
                directionData = enemyNev.FindPath(node, player.GetComponent<PlayerManager>().lastNode);
                for (int i = directionData.Length - 1; i > -1; i--)
                {
                    path.Push(directionData[i]);
                }
                if (path.Count > 3)
                {
                    keyDir = (Direction)path.Pop();
                }
                else
                {
                    state = State.Scattered;
                }
                break;
        }
    }

    void Retreat()
    {
        while (path.Count > 0)
        {
            path.Pop();
        }
        Vector3 target = Vector3.zero;
        if (player.GetComponent<PlayerManager>().nextNode != null) {
            target = player.GetComponent<PlayerManager>().nextNode.transform.position;
        }
        Vector3 escape = Vector3.Normalize(transform.position - target);
        bool[] escapeRoute = new bool[4];
        if (escape.x < 0) escapeRoute[0] = true;
        if (escape.x > 0) escapeRoute[1] = true;
        if (escape.y > 0) escapeRoute[2] = true;
        if (escape.y < 0) escapeRoute[3] = true;
        bool[] canMove = new bool[4];
        for (int i = 0; i < canMove.Length; i++)
        {
            //left right up down
            canMove[i] = (node.nextNode[i] != null);
        }
        do
        {
            keyDir = (Direction)Random.Range(0, 4); //0 left, 1 right, 2 up, 3 down
        } while (!canMove[(int)keyDir]);
        for (int i = 0; i < canMove.Length; i++)
        {
            if(canMove[i] && escapeRoute[i])
            {
                keyDir = (Direction)i;
            }
        }
        
    }

    public void ToStart()
    {
        if (node == null) return;
        if (path.Count == 0)
        {
            int[] directionData = enemyNev.FindPath(node, startNode.GetComponent<Node>());
            for (int i = directionData.Length-1; i > -1; i--) {
                path.Push(directionData[i]);
            }
        }
        if (path.Count > 0)
        {
            keyDir = (Direction)path.Pop();
        }
    }

}
