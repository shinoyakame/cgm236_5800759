﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

    public Animator animator;
    public float speed;
    public Node node;
    public Node lastNode;
    public Node nextNode;
    public enum Direction
    {
        Left,
        Right,
        Up,
        Down
    }
    public Direction moveDir = new Direction();
    public Direction keyDir = new Direction();
    public bool isHorizontalLen;

    public void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public virtual void Start()
    {
        isHorizontalLen = true;
    }

    public virtual void Update()
    {
        if (!GameManager.play)
        {
            animator.speed = 0;
            return;
        }
        animator.speed = 1;
        Animate();
    }

    public virtual void Move()
    {
        if (GameManager.death) return;
        Vector3 forward = Vector3.zero;
        switch (moveDir)
        {
            case Direction.Left:

                forward = Vector3.left;
                isHorizontalLen = true;
                nextNode = lastNode.nextNode[0];
                if (node != null)
                {
                    if (node.nextNode[0]==null)
                    {
                        forward = Vector3.zero;
                        animator.speed = 0;
                    }
                }
                break;
            case Direction.Right:


                forward = Vector3.right;
                isHorizontalLen = true;
                nextNode = lastNode.nextNode[1];
                if (node != null)
                {
                    if (node.nextNode[1] == null)
                    {
                        forward = Vector3.zero;
                        animator.speed = 0;
                    }
                }
                break;
            case Direction.Down:
                forward = Vector3.down;
                isHorizontalLen = false;
                nextNode = lastNode.nextNode[3];
                if (node != null)
                {
                    if (node.nextNode[3] == null)
                    {
                        forward = Vector3.zero;
                        animator.speed = 0;
                    }
                }
                break;
            case Direction.Up:
                forward = Vector3.up;
                isHorizontalLen = false;
                nextNode = lastNode.nextNode[2];
                if (node != null)
                {
                    if (node.nextNode[2] == null)
                    {
                        forward = Vector3.zero;
                        animator.speed = 0;
                    }
                }
                break;
            default:
                break;
        }
        transform.Translate(forward * speed * Time.deltaTime);
    }

    public void Animate()
    {
        animator.SetBool("left", false);
        animator.SetBool("right", false);
        animator.SetBool("up", false);
        animator.SetBool("down", false);
        switch (moveDir)
        {
            case Direction.Left:
                animator.SetBool("left", true);
                break;
            case Direction.Right:
                animator.SetBool("right", true);
                break;
            case Direction.Up:
                animator.SetBool("up", true);
                break;
            case Direction.Down:
                animator.SetBool("down", true);
                break;
        }
    }

    public virtual void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Node"))
        {
            transform.position = collider.transform.position;
            node = collider.GetComponent<Node>();
            lastNode = node;
        }
    }

    public virtual void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.CompareTag("Node"))
        {
            switch (keyDir)
            {
                case Direction.Left:
                    if (node.nextNode[0] != null)
                    {
                        moveDir = keyDir;
                    }
                    break;
                case Direction.Right:
                    if (node.nextNode[1] != null)
                    {
                        moveDir = keyDir;
                    }
                    break;
                case Direction.Up:
                    if (node.nextNode[2] != null)
                    {
                        moveDir = keyDir;
                    }
                    break;
                case Direction.Down:
                    if (node.nextNode[3] != null)
                    {
                        moveDir = keyDir;
                    }
                    break;
            }

        }
    }

    public virtual void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.CompareTag("Node"))
        {
            node = null;
        }
    }
}
