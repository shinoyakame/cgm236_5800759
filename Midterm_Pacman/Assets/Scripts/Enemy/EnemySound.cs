﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySound : MonoBehaviour {

    public AudioSource[] audioSource;
    public int level;

    public void Play(int index)
    {
        if (index < level) return;
        if (audioSource[index].isPlaying) return;
        audioSource[index].Play();
        level = index;
        for (int i = 0; i < index; i++)
        {
            Stop(i);
        }
    }

    public void Stop(int index)
    {
        audioSource[index].Stop();
    }

    public void StopAll()
    {
        for (int i = 0; i < audioSource.Length; i++)
        {
            audioSource[i].Stop();
            level = 0;
        }
    }

}
