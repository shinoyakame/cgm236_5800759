﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeManager : MonoBehaviour {

    public GameObject lifePrefab;
    public float lifeOffset;
    public static int life;
    public int Life
    {
        get
        {
            return life;
        }
        set
        {
            life = value;
            ShowLife();
        }
    }

    void Start()
    {
        Life = GameProgress.life;
    }

    void ShowLife()
    {
        Reset();
        for (int i = 0; i < Life; i++)
        {
            GameObject num = Instantiate(lifePrefab, transform.position, Quaternion.identity) as GameObject;
            num.transform.SetParent(transform);
            num.transform.Translate(i * lifeOffset, 0, 0);
        }
    }

    void Reset()
    {
        foreach (Transform child in transform)
        {
            DestroyImmediate(child.gameObject);
        }
    }

}
