﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : Character {

    public GameManager gameManager;
    public ScoreManager scoreManager;
    public AudioSource[] audioSource;
    public static int eatCount;

    public override void Start()
    {
        base.Start();
        transform.position = node.transform.position;
    }

    public override void Update()
    {
        base.Update();
        if (!GameManager.play) return;
        GetKey();
        Move();
    }

    void GetKey()
    {
        if(Input.GetAxisRaw("Horizontal") > 0){
            keyDir = Direction.Right;
            if (isHorizontalLen) moveDir = keyDir;
        }
        else if(Input.GetAxisRaw("Horizontal") < 0)
        {
            keyDir = Direction.Left;
            if (isHorizontalLen) moveDir = keyDir;
        }
        if (Input.GetAxisRaw("Vertical") > 0)
        {
            keyDir = Direction.Up;
            if (!isHorizontalLen) moveDir = keyDir;
        }
        else if (Input.GetAxisRaw("Vertical") < 0)
        {
            keyDir = Direction.Down;
            if (!isHorizontalLen) moveDir = keyDir;
        }
    }

    public override void OnTriggerEnter2D(Collider2D collider)
    {
        base.OnTriggerEnter2D(collider);
        if(collider.gameObject.name == "10point")
        {
            audioSource[0].Play();
            scoreManager.Score += 10;
            Eat();
            Destroy(collider.gameObject);
        }
        if (collider.gameObject.name == "50point")
        {
            scoreManager.Score += 50;
            Eat();
            gameManager.StartCoroutine("Retreat");
            Destroy(collider.gameObject);
        }
        if (collider.CompareTag("Enemy"))
        {
            if (collider.GetComponent<Enemy>().state == Enemy.State.Chased || collider.GetComponent<Enemy>().state == Enemy.State.Scattered)
            {
                gameManager.StartCoroutine("Death");
            }
            if (collider.GetComponent<Enemy>().state == Enemy.State.Retreat)
            {
                collider.GetComponent<Enemy>().StartCoroutine("Eaten");
            }
        }
    }

    void Eat()
    {
        eatCount++;
        if (eatCount >= 244)
        {
            gameManager.StartCoroutine("Clear");
        }
    }

}
