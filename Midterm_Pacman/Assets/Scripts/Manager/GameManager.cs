﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public Animator stage;
    public GameObject ready;
    public GameObject gameOver;
    public PlayerManager player;
    public Enemy blinky;
    public Enemy clyde;
    public Enemy inky;
    public Enemy pinky;
    public static bool play;
    public static bool death;
    public LifeManager lifeManager;
    Enemy[] enemyList;

    void Start()
    {
        enemyList = new Enemy[4]{blinky, clyde, inky, pinky};
        play = false;
        death = false;
        ready.SetActive(true);
        gameOver.SetActive(false);
        stage.speed = 0;
        Invoke("StartGame", 4f);
    }

    public IEnumerator Death()
    {
        StopCoroutine("Retreat");
        foreach(Enemy enemy in enemyList)
        {
            enemy.StopAllCoroutines();
            enemy.enemySound.StopAll();
        }
        death = true;
        play = false;
        player.animator.speed = 0;
        yield return new WaitForSeconds(2f);
        play = true;
        Destroy(blinky.gameObject);
        Destroy(clyde.gameObject);
        Destroy(inky.gameObject);
        Destroy(pinky.gameObject);
        player.audioSource[1].Play();
        player.animator.speed = 1;
        player.animator.SetTrigger("death");
        lifeManager.Life--;
        yield return new WaitForSeconds(4f);
        if (lifeManager.Life > 0)
        {
            GameProgress.score = ScoreManager.score;
            GameProgress.life = LifeManager.life;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        else
        {
            gameOver.SetActive(true);
        }
    }

    public IEnumerator Clear()
    {
        StopCoroutine("Retreat");
        foreach (Enemy enemy in enemyList)
        {
            enemy.StopAllCoroutines();
            enemy.enemySound.StopAll();
        }
        death = true;
        play = false;
        player.animator.speed = 0;
        yield return new WaitForSeconds(2f);
        Destroy(blinky.gameObject);
        Destroy(clyde.gameObject);
        Destroy(inky.gameObject);
        Destroy(pinky.gameObject);
        stage.speed = 1;
        yield return new WaitForSeconds(4f);
        stage.speed = 0;
        GameProgress.score = 0;
        PlayerManager.eatCount = 0;
        Destroy(DotProgress.dontDestroy.gameObject);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public IEnumerator Retreat()
    {
        if (blinky.state == Enemy.State.Scattered || blinky.state == Enemy.State.Chased)
        {
            blinky.animator.SetTrigger("retreat");
            blinky.state = Enemy.State.Retreat;
        }
        if (inky.state == Enemy.State.Scattered || inky.state == Enemy.State.Chased)
        {
            inky.animator.SetTrigger("retreat");
            inky.state = Enemy.State.Retreat;
        }
        if (pinky.state == Enemy.State.Scattered || pinky.state == Enemy.State.Chased)
        {
            pinky.animator.SetTrigger("retreat");
            pinky.state = Enemy.State.Retreat;
        }
        if (clyde.state == Enemy.State.Scattered || clyde.state == Enemy.State.Chased)
        {
            clyde.animator.SetTrigger("retreat");
            clyde.state = Enemy.State.Retreat;
        }
        yield return new WaitForSeconds(4f);

        if (blinky.state == Enemy.State.Retreat) blinky.animator.SetTrigger("blink");
        if (inky.state == Enemy.State.Retreat) inky.animator.SetTrigger("blink");
        if (pinky.state == Enemy.State.Retreat) pinky.animator.SetTrigger("blink");
        if (clyde.state == Enemy.State.Retreat) clyde.animator.SetTrigger("blink");
        yield return new WaitForSeconds(2f);

        if (blinky.state == Enemy.State.Retreat)
        {
            blinky.animator.SetTrigger("birth");
            blinky.state = Random.Range(0, 2) == 0 ? Enemy.State.Scattered : Enemy.State.Chased;
        }
        if (inky.state == Enemy.State.Retreat)
        {
            inky.animator.SetTrigger("birth");
            inky.state = Random.Range(0, 2) == 0 ? Enemy.State.Scattered : Enemy.State.Chased;
        }
        if (pinky.state == Enemy.State.Retreat)
        {
            pinky.animator.SetTrigger("birth");
            pinky.state = Random.Range(0, 2) == 0 ? Enemy.State.Scattered : Enemy.State.Chased;
        }
        if (clyde.state == Enemy.State.Retreat)
        {
            clyde.animator.SetTrigger("birth");
            clyde.state = Random.Range(0, 2) == 0 ? Enemy.State.Scattered : Enemy.State.Chased;
        }

    }

    void StartGame()
    {
        ready.SetActive(false);
        blinky.state = Enemy.State.ToStart;
        Invoke("ReleasePinky", 5f);
        play = true;
    }

    void ReleasePinky()
    {
        Invoke("ReleaseInky", 10f);
        pinky.state = Enemy.State.ToStart;
        pinky.ToStart();
    }

    public void ReleaseInky()
    {
        Invoke("ReleaseClyde", 13f);
        inky.state = Enemy.State.ToStart;
        inky.ToStart();
    }

    void ReleaseClyde()
    {
        clyde.state = Enemy.State.ToStart;
        clyde.ToStart();
    }

}
