﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ScoreManager : MonoBehaviour {

    public GameObject[] scorePrefab;
    public float numOffset;
    public static int score;
    public int Score {
        get {
            return score;
        }
        set {
            score = value;
            ShowScore();
        }
    }

    void Start()
    {
        Score = GameProgress.score;
    }

    void ShowScore()
    {
        Reset();
        char[] scoreChar = Score.ToString().ToCharArray();
        int[] scoreInt = new int[scoreChar.Length];
        for (int i = 0; i < scoreChar.Length; i++) {
            scoreInt[i] = (int)(scoreChar[i]) - 48;
        }
        for (int i = 0; i < scoreInt.Length; i++)
        {
            GameObject num = Instantiate(scorePrefab[scoreInt[i]], transform.position, Quaternion.identity) as GameObject;
            num.transform.SetParent(transform);
            num.transform.Translate(i * numOffset, 0, 0);
        }
        if (scoreInt.Length < 2)
        {
            GameObject num = Instantiate(scorePrefab[0], transform.position, Quaternion.identity) as GameObject;
            num.transform.SetParent(transform);
            num.transform.Translate(1 * numOffset, 0, 0);
        }
    }
    
    void Reset()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
}
