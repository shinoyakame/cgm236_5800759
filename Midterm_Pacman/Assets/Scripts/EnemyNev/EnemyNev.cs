﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyNev : MonoBehaviour {

    void ResetNodeData()
    {
        Node[] nodes = FindObjectsOfType<Node>();
        foreach(Node node in nodes)
        {
            node.isChecked = false;
            node.parent = null;
        }
    }

    /*void DepthFirstSearch(Node current, Node target)
    {
        current.isChecked = true;
        for(int i=0; i<current.nextNode.Length; i++)
        {
            if(current.nextNode[i] != null)
            {
                if (!current.nextNode[i].isChecked)
                {
                    current.nextNode[i].parent = current;
                    DepthFirstSearch(current.nextNode[i], target);
                }
            }
        }
    }*/

    void BreadthFirstSearch(Node current, Node target)
    {
        Queue<Node> Q = new Queue<Node>();
        Q.Enqueue(current);

        while(Q.Count > 0)
        {
            current = Q.Dequeue();
            current.isChecked = true;
            if (current == target)
            {
                return;
            }
            for(int i = 0; i < current.nextNode.Length; i++)
            {
                if (current.nextNode[i] != null)
                {
                    if (!current.nextNode[i].isChecked)
                    {
                        current.nextNode[i].parent = current;
                        Q.Enqueue(current.nextNode[i]);
                    }
                }
            }
        }
    }

    public int[] FindPath(Node start, Node target)
    {
        BreadthFirstSearch(start, target);
        List<Node> nodeList;
        nodeList = new List<Node>();
        Stack<int> directionStack;
        directionStack = new Stack<int>();
        Node current = target;
        nodeList.Add(current);
        while (current != start)
        {
            nodeList.Add(current.parent);
            current = current.parent;
        } 
        Node[] nodes = nodeList.ToArray();
        for(int i=0; i<nodes.Length-1; i++)
        {
            for(int j=0; j<nodes[i].parent.nextNode.Length; j++)
            {
                if (nodes[i].parent.nextNode[j] == nodes[i])
                {
                    directionStack.Push(j);
                }
            }
        }
        int[] directionData = new int[directionStack.Count];
        int count = 0;
        while(directionStack.Count > 0)
        {
            directionData[count] = directionStack.Pop();
            count++;
        }
        ResetNodeData();
        return directionData;
    }

}
