﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SpriteGenerator : MonoBehaviour {

    [Range(1, 150, order = 0)]
    [SerializeField]
    int horizontal;

    [Range(1, 150, order = 1)]
    [SerializeField]
    int vertical;

    [Range(0f, 10f, order = 2)]
    [SerializeField]
    float spacing;

    [Range(0.01f, 5f, order = 3)]
    [SerializeField]
    float scaling;

    [Range(-2f, 2f, order = 4)]
    [SerializeField]
    float skewing;

    public bool FlipX, FlipY;

    [HideInInspector]
    float tempspacing;

    [HideInInspector]
    float tempscaling;

    [HideInInspector]
    float tempskewing;

    [HideInInspector]
    public bool generating = false;

    public bool useLedge;
    public Vector2 ledgeOffset;
    public Vector2 ledgeSize;

    [Range(0f, 80f, order = 5)]
    public float ledgeDistance;

    [HideInInspector]
    Vector2 tempLedgeOffset;
    Vector2 tempLedgeSize;
    float tempLedgeDistance;

    public GameObject spritePrefab;
    public GameObject ledgePrefab;

    public void GenerateObjects()
    {
        Reset();
        CreateSpriteSet();
        CreateLedge();
    }

    void CreateLedge()
    {
        if (!useLedge) return;
        if (ledgePrefab == null) return;
        GameObject ledgeLeft = Instantiate(ledgePrefab, transform.position, Quaternion.identity) as GameObject;
        GameObject ledgeRight = Instantiate(ledgePrefab, transform.position, Quaternion.identity) as GameObject;
        ledgeLeft.transform.SetParent(transform);
        ledgeRight.transform.SetParent(transform);
        if (ledgePrefab.GetComponent<BoxCollider2D>() == null) return;
        ledgeLeft.GetComponent<BoxCollider2D>().size = ledgeSize;
        ledgeRight.GetComponent<BoxCollider2D>().size = ledgeSize;
        ledgeLeft.GetComponent<BoxCollider2D>().offset = ledgeOffset;
        ledgeRight.GetComponent<BoxCollider2D>().offset = ledgeOffset + new Vector2(ledgeDistance, 0);
    }

    void CreateSpriteSet()
    {
        if (spritePrefab == null) return;
        //create new sprite obj
        for (int i = 0; i < vertical; i++)
        {
            for (int j = 0; j < horizontal; j++)
            {
                GameObject child = Instantiate(spritePrefab, transform.position, Quaternion.identity) as GameObject;
                child.transform.SetParent(transform);
                child.transform.localPosition = new Vector3(j * spacing * scaling, -i * spacing * scaling - (j * skewing), 0);
                child.transform.localScale = new Vector3(scaling, scaling, 1);
                if (FlipX || FlipY)
                {
                    SpriteRenderer childSprite = child.GetComponent<SpriteRenderer>();
                    if (childSprite != null)
                    {
                        if (FlipX)
                            childSprite.flipX = true;
                        if (FlipY)
                            childSprite.flipY = true;
                    }
                }
            }
        }
    }

    public void Reset()
    {
        //destroy old
        while(transform.childCount != 0)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }
    }

    void Update()
    {
        if (generating)
        {
            if(tempspacing != spacing)
            {
                tempspacing = spacing;
                GenerateObjects();
                return;
            }
            if (tempscaling != scaling)
            {
                tempscaling = scaling;
                GenerateObjects();
                return;
            }
            if (tempskewing != skewing)
            {
                tempskewing = skewing;
                GenerateObjects();
                return;
            }
            if (tempLedgeOffset != ledgeOffset)
            {
                tempLedgeOffset = ledgeOffset;
                GenerateObjects();
                return;
            }
            if (tempLedgeSize != ledgeSize)
            {
                tempLedgeSize = ledgeSize;
                GenerateObjects();
                return;
            }
            if(tempLedgeDistance != ledgeDistance)
            {
                tempLedgeDistance = ledgeDistance;
                GenerateObjects();
                return;
            }
        }
    }

}
