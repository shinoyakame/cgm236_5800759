﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpriteGenerator))]
public class SpriteGeneratorEditor : Editor {
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        SpriteGenerator myScript = (SpriteGenerator)target;

        if (GUILayout.Button("Generate"))
        {
            myScript.GenerateObjects();
            myScript.generating = true;
        }
        if (GUILayout.Button("Reset"))
        {
            myScript.Reset();
            myScript.generating = false;
        }
    }

}
