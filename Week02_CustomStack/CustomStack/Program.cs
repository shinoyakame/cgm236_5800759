﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week02_CustomStack
{
    class Program
    {
        static void Main(string[] args)
        {
            CStack myStack = new CStack();

            string choice, value;

            string word = "InukameLighter";

            for(int i = 0; i < word.Length; i++)
            {
                myStack.Push(word.Substring(i, 1));
            }

            while (true)
            {
                Console.WriteLine("---------------------");
                Console.WriteLine("(t) top");
                Console.WriteLine("(p) pop");
                Console.WriteLine("(u) push");
                Console.WriteLine("(c) count");
                Console.WriteLine("---------------------");

                choice = Console.ReadLine();
                choice = choice.ToLower();

                char[] ccechar = choice.ToCharArray();

                switch (ccechar[0])
                {
                    case 't':
                        Console.WriteLine("top : " + myStack.Top());
                        break;
                    case 'p':
                        Console.WriteLine("popping : " + myStack.Pop());
                        break;
                    case 'u':
                        Console.WriteLine();
                        Console.WriteLine("Enter value to push : ");
                        value = Console.ReadLine();
                        myStack.Push(value);

                        Console.WriteLine("pushing : " + myStack.Top());
                        break;
                    case 'c':
                        Console.WriteLine("count : " + myStack.Count);
                        break;

                }
            }
        }
    }
}
