﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week05_BruteForce
{
    class Point
    {
        public float x;
        public float y;

        public Point()
        {
            Random rand = new Random();
            x = rand.Next(0, 800);
            y = rand.Next(0, 600);
        }
    }
}
