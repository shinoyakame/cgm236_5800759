﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week05_BruteForce
{
    class Program
    {
        static void Main(string[] args)
        {
            Point[] points = new Point[12];

            //Create Points
            for(int i=0; i<points.Length; i++)
            {
                points[i] = new Point();
                Console.WriteLine("X: " + points[i].x + "; Y: " + points[i].y);
                Console.ReadLine();
            }

            int index1 = 0, index2 = 0;
            double distmin;
            distmin = Double.PositiveInfinity;
            //Brute Force
            for(int i=0; i<points.Length-1; i++)
            {
                for(int j=i+1; j<points.Length; j++)
                {
                    double dist = (int)(Math.Sqrt(Math.Pow(points[i].x - points[j].x, 2) + Math.Pow(points[i].y - points[j].y, 2)));
                    if(dist < distmin)
                    {
                        distmin = dist;
                        index1 = i;
                        index2 = j;
                    }
                }
            }
            Console.WriteLine("The Closest Pair is " + index1.ToString() + " and " + index2.ToString());
            Console.ReadLine();
        }
    }
}
