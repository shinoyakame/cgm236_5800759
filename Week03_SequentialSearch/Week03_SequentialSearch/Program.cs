﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week03_SequentialSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                int[] array = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                Console.WriteLine("::Sequential Search::");
                Console.WriteLine("Type a number to search : ");

                int k;
                while (!int.TryParse(Console.ReadLine(), out k))
                {
                    Console.WriteLine("<Please insert number>");
                }

                int result = SequentialSearch(array, k);
                Console.WriteLine();

                if (result != -1)
                {
                    Console.WriteLine(k + " is at index '" + result + "'");
                }
                else
                {
                    Console.WriteLine(k + " is not found");
                }
                Console.WriteLine();
            }
        }

        static int SequentialSearch(int[] array, int k)
        {
            int index = 0;
            while (index < array.Length && array[index] != k)
            {
                index = index + 1;
            }
            if (index < array.Length)
            {
                return index;
            }
            else
            {
                return -1;
            }
        }
    }
}
