﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week03_GCD
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                int m, n;
                Console.WriteLine("Insert m value : ");
                while (!int.TryParse(Console.ReadLine(), out m) || m < 1)
                {
                    Console.WriteLine("<please type a positive and non-zero number>");
                }
                Console.WriteLine();
                Console.WriteLine("Insert n value : ");
                while (!int.TryParse(Console.ReadLine(), out n) || n < 1)
                {
                    Console.WriteLine("<please type a positive and non-zero number>");
                }
                Console.WriteLine();

                Console.WriteLine("The greatest common division value between " + m + " and " + n + " is " + gcd(m, n));
                Console.ReadLine();
            }
        }

        static int gcd(int m, int n)
        {
            int r;
            while (n != 0)
            {
                r = m % n;
                m = n;
                n = r;
            }
            return m;
        }
    }
}
