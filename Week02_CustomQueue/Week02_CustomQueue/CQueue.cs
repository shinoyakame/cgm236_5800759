﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Week02_CustomQueue
{
    class CQueue
    {
        ArrayList pqueue;

        public CQueue()
        {
            pqueue = new ArrayList();
        }

        public void Enqueue(Object item)
        {
            pqueue.Add(item);
        }
        public void Dequeue()
        {
            if (pqueue.Count != 0)
            {
                pqueue.RemoveAt(0);
            }
            else
            {
                Console.WriteLine("Queue is empty");
            }
        }
        public Object Front()
        {
            if (pqueue.Count != 0)
            {
                return pqueue[0];
            }
            else
            {
                return null;
            }
        }
        public int Count
        {
            get { return pqueue.Count; }
        }
        public void ClearQueue()
        {
            pqueue.Clear();
        }
    }
}
