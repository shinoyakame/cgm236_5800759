﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week02_CustomLinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList myList = new LinkedList();
            ListIter iter = new ListIter(myList);

            string choice, value;

            iter.InsertAfter("Subin");
            iter.InsertAfter("Wiwat");
            iter.InsertAfter("Somchai");
            iter.Reset();
            myList.ShowList();

            while (true)
            {
                Console.WriteLine("-------------------");
                Console.WriteLine("(n) move to next node ");
                Console.WriteLine("(g) get value in current node ");
                Console.WriteLine("(f) reset iterator ");
                Console.WriteLine("(r) remove current node ");
                Console.WriteLine("(s) show complete list ");
                Console.WriteLine("(a) InsertAfter ");

                choice = Console.ReadLine();
                choice = choice.ToLower();

                char[] onechar = choice.ToCharArray();

                switch (onechar[0])
                {
                    case 'n':
                        if ((!(myList.IsEmpty())) && (!(iter.AtEnd())))
                        {
                            iter.NextLink();
                        }
                        else
                        {
                            Console.WriteLine("Can't move to the next node");
                        }
                        break;
                    case 'g':
                        if (!myList.IsEmpty())
                        {
                            Console.WriteLine("Element : " + iter.GetCurrent().Element);
                        }
                        else
                        {
                            Console.WriteLine("List is Empty");
                        }
                        break;
                    case 'f':
                        iter.Reset();
                        break;
                    case 'r':
                        if (iter.GetCurrent() == myList.GetFirst())
                        {
                            Console.WriteLine("Can't remove header");
                        }
                        else
                        {
                            iter.Remove();
                            iter.NextLink();
                        }
                        break;
                    case 's':
                        if (!myList.IsEmpty())
                        {
                            myList.ShowList();
                        }
                        else
                        {
                            Console.WriteLine("List is empty");
                        }
                        break;
                    case 'a':
                        Console.WriteLine();
                        Console.WriteLine("Enter value to insert");
                        value = Console.ReadLine();
                        iter.InsertAfter(value);
                        break;
                }
            }

        }
    }
}
