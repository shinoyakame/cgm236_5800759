﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week02_CustomLinkedList
{
    public class ListIter
    {
        private Node current;
        private Node previous;
        LinkedList theList;

        public ListIter(LinkedList list)
        {
            theList = list;
            current = theList.GetFirst();
            previous = null;
        }

        public void NextLink()
        {
            previous = current;
            current = current.Link;
        }

        public Node GetCurrent()
        {
            return current;
        }

        public void InsertAfter(Object theElement)
        {
            Node newNode = new Node(theElement);
            newNode.Link = current.Link; //to next node
            current.Link = newNode; //to new node
            NextLink(); //move 1 node
        }

        public void Remove()
        {
            previous.Link = current.Link;
        }

        public void Reset()
        {
            current = theList.GetFirst();
            previous = null;
        }

        public bool AtEnd()
        {
            return (current.Link == null);
        }
    }
}
