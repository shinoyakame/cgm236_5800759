﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5800759_AI_Week01
{
    class CArray
    {
        int[] arr;
        int upper;
        int numElements;

        public CArray(int size)
        {
            arr = new int[size];
            upper = size - 1;
            numElements = 0;
        }

        public void Insert(int item)
        {
            arr[numElements] = item;
            numElements++;
        }

        public void DisplayElements()
        {
            for (int i = 0; i <= upper; i++)
            {
                Console.Write(arr[i] + " ");
            }
            Console.Write(Environment.NewLine);
        }

        public void Clear()
        {
            for (int i = 0; i <= upper; i++)
                arr[i] = 0;
            numElements = 0;
        }

        public void BubbleSort()
        {
            int temp, outer, inner;

            for(outer=upper; outer >= -1; outer--)
            {
                for(inner = 0; inner <= outer - 1; inner++)
                {
                    if((int)arr[inner] > arr[inner + 1])
                    {
                        temp = arr[inner];
                        arr[inner] = arr[inner + 1];
                        arr[inner + 1] = temp;
                    }
                }

                this.DisplayElements();
            }
        }

        public void SelectionSort()
        {
            int min, outer, inner, temp;

            for(outer = 0; outer <= upper; outer++)
            {
                min = outer;
                for(inner = outer + 1; inner <= upper; inner++)
                {
                    if (arr[inner] < arr[min])
                        min = inner;
                    
                }
                temp = arr[outer];
                arr[outer] = arr[min];
                arr[min] = temp;
            }
        }

        public void InsertionSort()
        {
            int inner, outer, temp;

            for (outer = 1; outer <= upper; outer++)
            {
                temp = arr[outer];
                inner = outer;

                while (inner > 0 && arr[inner - 1] >= temp)
                {
                    arr[inner] = arr[inner - 1];
                    inner -= 1;
                }
                arr[inner] = temp;
            }
        }
    }
}
