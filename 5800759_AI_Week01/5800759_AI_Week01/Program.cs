﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5800759_AI_Week01
{
    class Program
    {
        static void Main(string[] args)
        {
            CArray nums = new CArray(10);

            Random random = new Random(100);

            for (int i = 0; i < 10; i++)
                nums.Insert((int)(random.NextDouble() * 100));

            Console.WriteLine("Before sorting : ");
            nums.DisplayElements();

            //Console.WriteLine("");
            //Console.WriteLine("During sorting : ");
            //nums.BubbleSort();

            nums.SelectionSort();

            //nums.InsertionSort();

            Console.WriteLine("");
            Console.WriteLine("After sorting : ");
            nums.DisplayElements();

            Console.Read();
        }
    }
}
